$(document).ready(function(){

	// $(".openMore").click(function(e){
	// 	e.preventDefault()
	// 	$( "."+$(this).attr('data-open') ).width('50%')
	// 	$( "."+$(this).attr('data-open')+" .content" ).show()
	// })

	// $('.service-more .closeServMore').click(function(){
	// 	$(this).parents('.service-more').width(0);
	// 	$(this).parent().hide()
	// })

	if( $(window).width() > 992 ){
		var startScrollTop = $(this).scrollTop();
		$(window).scroll(function(){
			var currScrollTop = $(this).scrollTop();
			if( currScrollTop > startScrollTop ){
				$('.top-panel').hide()
				startScrollTop = currScrollTop
			}else{
				$('.top-panel').show()
				startScrollTop = currScrollTop
			}
		})
	}
	$('.menuBtn').click(function(){
		$('.top-panel').slideToggle()
	});
	$('.menu>li>span').click(function(){
		$(this).next().slideToggle()
	})

	$('.tech-container .item').height( $('.tech-container .item').width() )


	$('.tabBtns li').each(function(i){
		$(this).attr('data-tab', i);
	})
	$('.tabContent').each(function(i){
		$(this).attr('data-tab', i);
	})
	$('.tabContent:first').show()

	$('.tabBtns li').click(function(){
		var thisTab = $(this).attr('data-tab');
		$('.tabContent').hide()
		$('.tabContent[data-tab='+thisTab+']').show()
	})

	$('.portfolio-item').magnificPopup({
		type: 'image',
		gallery:{
			enabled:true
		}
	});


})
